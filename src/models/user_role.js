'use strict';
module.exports = (sequelize, DataTypes) => {
  var User_Role = sequelize.define('User_Role', {
    id: {
      type: DataTypes.STRING,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      autoIncrement: false
    },
    name: DataTypes.STRING,
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {});
  User_Role.associate = function (models) {

    User_Role.hasOne(models.User)

  };
  return User_Role;
};
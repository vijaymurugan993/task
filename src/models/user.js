'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    id: {
      type: DataTypes.STRING,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      autoIncrement: false
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false, // first_name is not null
    },
    last_name: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      allowNull: false, // email is not null
      validate: {
        isEmail: true, // email validation, Given input is email or not
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false, // password is not null
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {});
  User.associate = function (models) {

  };
  return User;
};
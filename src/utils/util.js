'use strict'

const bcrypt = require('bcryptjs');

module.exports = {

    hashPassword: async (password) => {
        try {
            const salt = await bcrypt.genSaltSync(10);
            return await bcrypt.hash(password, salt);
        } catch (error) {
            throw new Error("hasing falid", error);
        }
    }

}
'use strict'

const Models = require('../models');

module.exports = {

    adminRole: () => {
        return new Promise(async (resolve, reject) => {
            let role = await Models.User_Role.findOne({
                where: {
                    isActive: true,
                    name: process.env.admin // get role name from env file
                },
                raw: true // return raw json output
            });

            if (role) {
                resolve(role.id);
            } else {
                reject(false) // if there is no role with admin return false
            }
        })
    },

    userRole: () => {
        return new Promise(async (resolve, reject) => {
            let role = await Models.User_Role.findOne({
                where: {
                    isActive: true,
                    name: process.env.user // get role name from env file
                },
                raw: true // return raw json output
            });

            if (role) {
                resolve(role.id);
            } else {
                reject(false) // if there is no role with admin return false
            }
        })
    }

}
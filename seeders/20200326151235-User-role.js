'use strict';

var Models = require('../src/models')

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('User_Roles', [{
      id: '1f110ed3-8541-435a-8ccb-3df7fea2fe2d',
      name: 'admin',
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: '3d955525-caba-497c-ac2f-18bba8e6eba5',
      name: 'user',
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  }
};
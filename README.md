
1. Server running on port 8080.
2. You can change in bin/www.
3. after running server need to add seed data like UserRoles.

   cmd: sequelize db:seed:all

APIs:

1. Add User

POST: http://localhost:8080/user
RESPONSE: {
	"first_name": "vijay",
	"last_name": "murugan",
	"email": "rvm993@gmail.com",
	"password": "123456"
}

2. Get user by Id

GET: http://localhost:8080/user/:userId
RESPONSE: {
    "id": "a8e47307-c51c-445c-83bf-be86c732b606",
    "first_name": "vijay",
    "last_name": "murugan",
    "email": "rvm993@gmail.com",
    "password": "$2a$10$YWPmEiHU3lZifl5I.wH/IuA.67hlu71yKljDTCe.RuU7ep4e040Bq",
    "isActive": true,
    "createdBy": null,
    "updatedBy": null,
    "createdAt": "2020-03-27T07:10:53.000Z",
    "updatedAt": "2020-03-27T07:10:53.000Z",
    "UserRoleId": "1f110ed3-8541-435a-8ccb-3df7fea2fe2d"
}

3. Get Users data
GET: http://localhost:8080/users
RESPONSE: [
    {
        "id": "68089725-1aa2-4e43-94ba-9f2e6542d1fa",
        "first_name": "vijay",
        "last_name": "murugan",
        "email": "rvm993@gmail.com",
        "password": "$2a$10$DO9LHFEjLbG9j8m3pjxaUOwhyBiIqZw9MFRGEMaY22H48hjgghyxO",
        "isActive": true,
        "createdBy": null,
        "updatedBy": null,
        "createdAt": "2020-03-27T07:19:07.000Z",
        "updatedAt": "2020-03-27T07:19:07.000Z",
        "UserRoleId": "3d955525-caba-497c-ac2f-18bba8e6eba5"
    },
    {
        "id": "cd5e2d7b-5652-437e-bb55-e165d5d58324",
        "first_name": "vijay",
        "last_name": "murugan",
        "email": "rvm993@gmail.com",
        "password": "$2a$10$k1.6CzN9i4viq2NqP84bX.0frjGCd9GH7sbCNx8skmX0xoGLlBL1i",
        "isActive": true,
        "createdBy": null,
        "updatedBy": null,
        "createdAt": "2020-03-27T07:19:10.000Z",
        "updatedAt": "2020-03-27T07:19:10.000Z",
        "UserRoleId": "3d955525-caba-497c-ac2f-18bba8e6eba5"
    }
]

4. Get Admins

GET: http://localhost:8080/admin-users
RESPONSE: [
    {
        "id": "a8e47307-c51c-445c-83bf-be86c732b606",
        "first_name": "vijay",
        "last_name": "murugan",
        "email": "rvm993@gmail.com",
        "password": "$2a$10$YWPmEiHU3lZifl5I.wH/IuA.67hlu71yKljDTCe.RuU7ep4e040Bq",
        "isActive": true,
        "createdBy": null,
        "updatedBy": null,
        "createdAt": "2020-03-27T07:10:53.000Z",
        "updatedAt": "2020-03-27T07:10:53.000Z",
        "UserRoleId": "1f110ed3-8541-435a-8ccb-3df7fea2fe2d"
    }
]

5. Update user
PUT: http://localhost:8080/user
RESPONSE: "User updated successfully!"

6. Delete User
DELETE: http://localhost:8080/user/:UserRoleId
RESPONSE: "User deleted successfully"